LPLolliInt: A Lambda Prolog Lolli Intrepter
===========================================


Notice
------

This project is experimental.

Synopsis
--------

Lolli is a fragment of Linear Logic developed by Hodas et al [1].
The metatheory of Lolli admits uniform provability permitting its interpretation as an abstract programming language [2].
The bulk of this interpreter was implemented by Dale Miller [3].
It has been modified to work with the Teyjus Lambda Prolog Implementation [4].

This interpreter is currently being extended in two ways.
The first is to allow proof objects to be recovered from a successful query.
The second is to permit proof construction up to some fixed depth.
When the depth is fixed it is possible to generate partial "proofs" for formulas that cannot be proven given a sufficient depth. 

Requirements/Building
---------------------
A working installation of Teyjus [4].
The lolli module can be built be running make.
The Makefile assumes tjcc, tjsim, and tjdepend are in the working path.


lolli
-----

Load the lolli interpreter by running 'tjsim lolli'.
A formula G may be proved by type 'go G P anydepth'.
If a proof exists, a proof object will be unified with P.
Limiting the depth can be done using the unary functor 'depth' applied to a non-negative integer.

imp
---

One example has been provided in the imp directory.
It describes the evaluation semantics for a small imperative programming language.
The language is defined in the comment in imp/impparse/impparse.
The python program imp/impparse/impparse will convert a program to a formula that may be queried.
Examine the impparse usage for more details.
The bash script imp/proof/proof will accept an input program, convert that input program to a formula using imp/impparse/impparse, excute a query in the lolli interpreter and out the proof if it exists.
The python program imp/proof/print will take a proof object and convert it to proof using bussproof commands.
Build the imp module can be done by entering the imp directory and running 'make'.
You may load the imp interpreter by running 'tjsim imp'.

References
----------

1 - Logic Programming in a Fragment of Intuitionistic Linear Logic, by Joshua S. Hodas and Dale Miller, Information and Computation, Vol. 110, No. 2, May 1, 1994, pp. 327-365.

2 - Uniform proofs as a foundation for logic programming, by Dale Miller, Gopalan Nadathur, Frank Pfenning, and Andre Scedrov. Annals of Pure and Applied Logic, Vol. 51 (1991), 125 - 157.

3 - http://www.lix.polytechnique.fr/Labo/Dale.Miller/lProlog/examples/lolli-interp/lolli-interp.html

4 - https://code.google.com/p/teyjus/