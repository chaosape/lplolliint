.PHONY: all lolli
all: lolli
lolli: lolli.lp

%.lpo : %.mod %.sig
	tjcc $*

%.lp : %.lpo
	tjlink $*

-include depend
depend: *.mod *.sig 
	tjdepend *.mod > depend-stage
	mv depend-stage depend

.PHONY: clean
clean:
	rm -f *.lpo *.lp depend
	(cd imp && make clean)
