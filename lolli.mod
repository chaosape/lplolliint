module lolli.

isR erase.
isR B           :- isA B.
isR (B1 & B2)   :- isR B1,  isR B2.
isR (B1 -o B2)  :- isG B1,  isR B2.
isR (B1 imp B2) :- isG B1,  isR B2.
isR (pi B)      :- pi x\(isR (B x)).
  
isG one.
isG erase.
isG B           :- isA B.
isG (B1 -o B2)  :- isR B1, isG B2.
isG (B1 imp B2) :- isR B1, isG B2.
isG (B1 &  B2)  :- isG B1, isG B2.
isG (B1 x  B2)  :- isG B1, isG B2.
isG (bang B)    :- isG B.
isG (pi B)      :- pi x\(isG (B x)).

reducedepth anydepth  anydepth.
reducedepth (depth D) (depth D') :- D' is D - 1.

type onlybangs context -> o.
onlybangs empty.
onlybangs ((bang _) ++ I) :- onlybangs I.

type split context -> context -> context -> o.
split empty empty empty.
split (H ++ T) (H ++ J) K        :- split T J K. 
split (H ++ T) J        (H ++ K) :- split T J K.

prove  I      _         maxdepth                                     (depth D)       :- D =< 0.
prove  I      one       (unary oneR (sequent I one) axiom)           D               :- onlybangs I.
prove  I      erase     (unary eraseR (sequent I erase) axiom)       D.
prove  I      (G1 & G2) (binary ampR (sequent I (G1 & G2)) P1 P2)    D               :- reducedepth D D', prove I G1 P1 D',  prove I G2 P2 D'.
prove  I      (R -o G)  (unary limpR (sequent I (R -o G)) P)         D               :- reducedepth D D', prove (R ++ I) G P D'.
prove  I      (R imp G) (unary impR (sequent I (R imp G)) P)         D               :- reducedepth D D', prove ((bang R) ++ I) G P D'.
prove  I      (G1 x G2) (binary tensR (sequent I (G1 x G2)) P1 P2)   D               :- reducedepth D D', split I J K, prove J G1 P1 D', prove K G2 P2 D'.
prove  I      (bang G)  (unary bangR (sequent I (bang G)) P)         D               :- reducedepth D D', onlybangs I, prove I G P D'.
prove  I      (pi   G)  (unary piR (sequent I (pi G)) P)             D               :- reducedepth D D', pi x\(prove I (G x) P D').
prove  I      A         (unary bcU (sequent I A) P)                  D               :- reducedepth D D', isA A, (A <- G), prove I G P D'.
prove  I      A         (unary bcU (sequent I A) P)                  D               :- reducedepth D D', isA A, pickUR I M R, bc M A R P D'.
prove  I      A         (unary bcB (sequent I A) P)                  D               :- reducedepth D D', isA A, pickBR I M R, bc M A R P D'.
prove  empty  (X = Y)   (unary eqA (sequent empty (X = Y)) axiom)    D               :- X is Y.
prove  empty  (X > Y)   (unary gtA (sequent empty (X > Y)) axiom)    D               :- X > Y.
prove  empty  (X =< Y)  (unary leqA (sequent empty (X =< Y)) axiom)  D               :- X =< Y.
prove  empty  (X <> Y)  (unary neqA (sequent empty (X <> Y)) axiom)  D               :- X < Y ; X > Y.
 
bc empty  A A          absorb        D.
bc I      A (G -o R)   P2            D               :- reducedepth D D', split I J K, bc J A R P1 D', prove K G P2 D'.
bc I      A (G imp R)  P2            D               :- reducedepth D D', split I J K, bc J A R P1 D', prove K G P2 D'.
bc I      A (pi R)     P             D               :- reducedepth D D', bc I A (R T) P D'.
bc I      A (R1 & R2)  P             D               :- reducedepth D D', (bc I A R1 P D'; bc I A R2 P D').
  
pickUR ((bang R) ++ I) ((bang R) ++ I) R.
pickUR (S ++ I) (S ++ O) R :- pickUR I O R.

pickBR (R ++ I) I R :- isR R.
pickBR (S ++ I) (S ++ O) R :- pickBR I O R.

 
type notA   o -> o.

notA erase.
notA one.
notA (X & Y).
notA (X x Y).
notA (X -o Y).
notA (X imp Y).
notA (bang X).
notA (pi X).
notA (X = Y).
notA (X > Y).
notA (X =< Y).
notA (X <> Y).

isA A  :- not (notA A).

go G P D :- prove empty G P D.
