sig imp.

accum_sig lolli.

kind term    type.
type num     int -> term.
type add     term -> term -> term.
type sub     term -> term -> term.
type gt      term -> term -> term.
type get     term -> term.
type set     term -> term -> term.
type seq     term -> term -> term.
type wh      term -> term -> term.

type e       term -> int -> o -> o.
type peano   int -> o.
type sum     int -> int -> o.
type m       int -> int -> o.

type wfsub int -> int -> int -> o.

