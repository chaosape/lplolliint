module imp.

accumulate lolli.



e (num N) N C <- C.
e (add E1 E2) N3 C <- e E1 N1 (e E2 N2 (N3 = N1 + N2 x C)).
e (sub E1 E2) N3 C <- e E1 N1 (e E2 N2 (wfsub N1 N2 N3 x C)).
e (gt E1 E2) 1 C <- e E1 N1 (e E2 N2 (N1 > N2 x C)).
e (gt E1 E2) 0 C <- e E1 N1 (e E2 N2 (N1 =< N2 x C)).
e (get E) N' C <- e E N (m N N' x (m N N' -o C)).
e (set E1 E2) N2 C <- e E1 N1 (e E2 N2 (m N1 N3 x (m N1 N2 -o C))).
e (seq E1 E2) N2 C <- e E1 N1 (e E2 N2 C).
e (wh E1 E2) 0 C <-  (e E1 N1 ((N1 <> 1) x C)).
e (wh E1 E2) N3 C <- e E1 N1 (N1 = 1 x (e E2 N2 (e (wh E1 E2) N3 C))).

sum 0 0 <- one.
sum I S <- S is S' + I x I' is I' - 1 x sum I' S'.

wfsub X Y 0 <- Y > X.
wfsub X Y Z <- Y =< X x Z = X - Y.