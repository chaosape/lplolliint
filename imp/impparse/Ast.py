class Node:

    def toString(self):
        return "toString: unimplemented"


class Program(Node):

    def __init__(self,memory,expression):
        self.memory = memory
        self.expression = expression

    def toString(self):
        return "(e %s R erase)"%(self.expression.toString())

class BinOp(Node):

    def __init__(self,left,op,right):
        self.left = left
        self.op = op
        self.right = right

    def __opToAST__(self):
       return {
           '+' : 'add',
           '-' : 'sub',
           '>' : 'gt',
           '<-' : 'set',
           'while' : 'wh',
           ';' : 'seq'
       }.get(self.op,'BADOP')

    def toString(self):
        return '(%s %s %s)'%(self.__opToAST__(),self.left.toString(),self.right.toString())

class DeRef(Node):
    
    def __init__(self,exp):
        self.exp = exp

    def toString(self):
        return '(get %s)'%(self.exp.toString())


class Natural(Node):
    
    def __init__(self,nat):
        self.nat = nat

    def toString(self):
        return '(num %i)'%(self.nat)


class Name(Node):
    
    def __init__(self,name):
        self.name = name

    def toString(self):
        return '%s'%self.name


class Group(Node):

    def __init__(self,exp):
        self.exp = exp

    def toString(self):
        return self.exp.toString()
