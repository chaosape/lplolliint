sig lolli.

type one      o.
type erase    o.
type bang     o -> o.
type -o       o -> o -> o.
type x        o -> o -> o.
type <-       o -> o -> o.
type imp      o -> o -> o.
type <>       int -> int -> o.


infix   <>  6.
infixr  -o  5.
infixr  x   5.
infix   <-  2.
infix   imp 2.



kind   context  type.
type   empty    context.
type   del      context -> context.
type   ++     o -> context -> context.
infixr ++     5.


type isR         o -> o.
type isG         o -> o.
type isA         o -> o.
type prove       context -> o -> proof -> depth -> o.
type pickUR      context -> context -> o -> o.
type pickBR      context -> context -> o -> o.
type bc          context -> o -> o -> proof -> depth -> o.
type reducedepth depth -> depth -> o.

kind sequent        type.
type sequent       context -> o -> sequent.

kind rule       type.
type tensR      rule.
type limpR      rule.
type impR       rule.
type oneR       rule.
type eraseR     rule.
type ampR       rule.
type bangR      rule.
type piR        rule.
type bcU        rule.
type bcB        rule.
type eqA        rule.
type gtA        rule.
type leqA       rule.
type neqA       rule.

kind proof type.
type axiom           proof.
type absorb          proof.
type maxdepth        proof.
type unary           rule -> sequent -> proof -> proof.
type binary          rule -> sequent -> proof -> proof -> proof.

kind depth type.
type depth int -> depth.
type anydepth depth.

type go o -> proof -> depth -> o.